# Network and Utilities Library
Python 3.7.x+ Network and Utilities Library.

## Getting Started

### Installation
#### Ubuntu 18.04 (Debian-based Linux)
```shell script
python3.7 -m pip install git+ssh://git@gitlab.com/<path>
```
```shell script
python3.7 -m pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>
```
#### Windows 10
```shell script
pip install git+ssh://git@gitlab.com/<path>
```
```shell script
pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>
```
#### Requirements.txt
For development of other projects
```
networkandutilitiespy@ git+ssh://git@gitlab.com/ernest-libraries/network-and-utilities-library-python
```

### Basic Usage
#### HTTP Component (Sync)
```python
from networkandutilitiespy.sockets import HTTP

http = HTTP()                                            # Localhost connection

response = http.head(url='http://httpbin.org/')          # HTTP HEAD request
response = http.get(url='http://httpbin.org/get')        # HTTP GET request
response = http.post(url='http://httpbin.org/post')      # HTTP POST request
response = http.put(url='http://httpbin.org/put')        # HTTP PUT request
response = http.delete(url='http://httpbin.org/delete')  # HTTP DELETE request
```

#### HTTP Component (Async)
```python
from networkandutilitiespy.sockets import HTTP

http = HTTP()

# HTTP GET async request
get_response = await http.get_async('http://httpbin.org/get')
print(await get_response.text())
# HTTP POST async request
post_response = await http.post_async('http://httpbin.org/post', data=b'data')
print(await post_response.text())
```

#### WebSocket Component
```python
from networkandutilitiespy.sockets import WebSocket

websocket = WebSocket()                                  # Initialise the WebSocket facade

await websocket.connect(uri='ws://echo.websocket.org/')  # Establish a WebSocket connection
await websocket.send('Hello World')                      # Send a message
response = await websocket.receive()                     # Receive message
await websocket.close()                                  # Close the WebSocket connection
```

#### Logger Component
```python
import logging
from networkandutilitiespy.utils import custom_logger

log = custom_logger(__name__, to_file=False, log_level=logging.INFO)                    # No .log file
log = custom_logger(__name__, filename='my_logs', mode='w+', log_level=logging.INFO)    # Output .log file

log.debug('debug log message')                                                          # DEBUG level message
log.info('info log message')                                                            # INFO level message
log.warning('warning log message')                                                      # WARNING level message
log.error('error log message')                                                          # ERROR level message
log.critical('critical log message')                                                    # CRITICAL level message
```
