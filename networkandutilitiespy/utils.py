"""Utility functions.

Classes
=======
1. AbstractAttribute
    Class used when defining an attribute that is expected to be implemented on sub-classes.

Functions
=========
1. custom_logger(name, to_file=True, filename=DEFAULT_LOG_FILENAME, mode='a', log_level=logging.INFO)
    Customise an instance of the Python Standard Library logger and returns the customised logger.
"""
import os
import logging

DEFAULT_LOG_FILENAME = './networkandutilitiespy.log'


class AbstractAttribute(object):
    """Class used when defining an attribute that is expected to be implemented on sub-classes."""

    def __get__(self, obj, obj_type):
        for cls in obj_type.__mro__:
            for name, value in cls.__dict__.items():
                if value is self:
                    this_obj = obj if obj else obj_type

                    raise NotImplementedError(f'{this_obj} does not have the attribute {name}. '
                                              f'Abstract Attribute from {cls.__name__}.')

        raise NotImplementedError(f'{obj_type.__name__} does not set the abstract attribute <unknown>.')


def custom_logger(name, to_file=True, filename=DEFAULT_LOG_FILENAME, mode='a', log_level=logging.INFO):
    """Customise an instance of the Python Standard Library logger and returns the customised logger.

    Parameters
    ==========
    :param name: Name of the logger.
    :type name: str
    :param to_file: Whether to save the log to a file.
    :type to_file: bool
    :param filename: Name of the log file.
    :type filename: str
    :param mode: The opening file mode e.g. r, w, a, r+, w+, a+.
    :type mode: str
    :param log_level: Any of the predefined logging levels of Python Standard Library
    :type log_level: int

    Returns
    =======
    :return: Customised logger.
    :rtype: logging.Logger
    """
    log_format = '%(asctime)s - %(levelname)s - %(processName)s - %(module)s - %(funcName)s - %(message)s'
    formatter = logging.Formatter(fmt=log_format)

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(log_level)
    logger.addHandler(handler)

    if to_file:
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        file_handler = logging.FileHandler(filename, mode=mode)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    return logger


__all__ = [
    'AbstractAttribute',
    'custom_logger',
]
