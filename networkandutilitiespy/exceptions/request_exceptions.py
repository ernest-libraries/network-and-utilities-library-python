from requests.exceptions import ConnectTimeout, ReadTimeout


__all__ = (
    'ConnectTimeout',
    'ReadTimeout',
)
