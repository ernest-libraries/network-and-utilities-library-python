"""HTTP Exception classes.

Classes
=======
1. HTTPError
2. HTTPBadRequestError
3. HTTPUnauthorizedError
4. HTTPForbiddenError
5. HTTPNotFoundError
6. HTTPRequestTimeoutError
7. HTTPInternalServerError
8. HTTPNotImplementedError
9. HTTPBadGatewayError
10. HTTPServiceUnavailableError
11. HTTPUnexpectedError
"""
from http import HTTPStatus

from networkandutilitiespy.utils import AbstractAttribute


class HTTPError(Exception):
    """Base exception class for HTTP errors."""

    DEFAULT_STATUS_CODE = AbstractAttribute()
    DEFAULT_MESSAGE = AbstractAttribute()

    def __init__(self, status_code=None, message=None, override_default_message=False, errors=None):
        """Set the error message based on the message and/or sequence of errors.

        Parameters
        ==========
        :param status_code: HTTP Status Code as specified by the IETF HTTP/1.1 RFC 2616.
        :type status_code: int
        :param message: Error message.
        :type message: str
        :param errors: Additional error descriptions provided by the server in its HTTP response.
        :type errors: list(str) or list(int) or tuple(str, str) or tuple(int, int)
        """
        if status_code is None:
            self.status_code = self.DEFAULT_STATUS_CODE
        else:
            self.status_code = status_code

        if message is None:
            self.message = self.DEFAULT_MESSAGE
        else:
            if override_default_message:
                self.message = message
            else:
                try:
                    self.message = f'{self.DEFAULT_MESSAGE}, {message}'
                except NotImplementedError:
                    self.message = message

        self.errors = errors
        if errors is not None:
            self.message += ': ' + ', '.join(errors)


class HTTPBadRequestError(HTTPError):
    """Exception class for an HTTP 400 Bad Request Error."""

    DEFAULT_STATUS_CODE = HTTPStatus.BAD_REQUEST
    DEFAULT_MESSAGE = '400 Bad Request'


class HTTPUnauthorizedError(HTTPError):
    """Exception class for an HTTP 401 Unauthorized Error."""

    DEFAULT_STATUS_CODE = HTTPStatus.UNAUTHORIZED
    DEFAULT_MESSAGE = '401 Unauthorized'


class HTTPForbiddenError(HTTPError):
    """Exception class for an HTTP 403 Forbidden Error."""

    DEFAULT_STATUS_CODE = HTTPStatus.FORBIDDEN
    DEFAULT_MESSAGE = '403 Forbidden'


class HTTPNotFoundError(HTTPError):
    """Exception class for an HTTP 404 Not Found Error."""

    DEFAULT_STATUS_CODE = HTTPStatus.NOT_FOUND
    DEFAULT_MESSAGE = '404 Not Found'


class HTTPRequestTimeoutError(HTTPError):
    """Exception class for an HTTP 408 Request Timeout Error."""

    DEFAULT_STATUS_CODE = HTTPStatus.REQUEST_TIMEOUT
    DEFAULT_MESSAGE = '408 Request Timeout'


class HTTPInternalServerError(HTTPError):
    """Exception class for an HTTP 500 Internal Server Error Error."""

    DEFAULT_STATUS_CODE = HTTPStatus.INTERNAL_SERVER_ERROR
    DEFAULT_MESSAGE = '500 Internal Server Error'


class HTTPNotImplementedError(HTTPError):
    """Exception class for an HTTP 501 Not Implemented Error."""

    DEFAULT_STATUS_CODE = HTTPStatus.NOT_IMPLEMENTED
    DEFAULT_MESSAGE = '501 Not Implemented'


class HTTPBadGatewayError(HTTPError):
    """Exception class for an HTTP 502 Bad Gateway Error."""

    DEFAULT_STATUS_CODE = HTTPStatus.BAD_GATEWAY
    DEFAULT_MESSAGE = '502 Bad Gateway'


class HTTPServiceUnavailableError(HTTPError):
    """Exception class for an HTTP 503 Service Unavailable."""

    DEFAULT_STATUS_CODE = HTTPStatus.SERVICE_UNAVAILABLE
    DEFAULT_MESSAGE = '503 Service Unavailable'


class HTTPUnexpectedError(HTTPError):
    """Exception class for an unexpected HTTP Error."""

    DEFAULT_STATUS_CODE = 0
    DEFAULT_MESSAGE = '0 Unexpected Error'


__all__ = (
    'HTTPError',
    'HTTPBadRequestError',
    'HTTPUnauthorizedError',
    'HTTPForbiddenError',
    'HTTPNotFoundError',
    'HTTPRequestTimeoutError',
    'HTTPInternalServerError',
    'HTTPNotImplementedError',
    'HTTPBadGatewayError',
    'HTTPServiceUnavailableError',
    'HTTPUnexpectedError',
)
