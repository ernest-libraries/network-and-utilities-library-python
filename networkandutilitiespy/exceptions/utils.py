"""Utility functions.

Functions
=========
1. raise_http_error(status_code: int, message: str = None)
    Raise an exception based on the error status code from an HTTP response.
    Include additional message taken from the response bondy content.
"""
from networkandutilitiespy.exceptions.http_exceptions import *


def raise_http_error(status_code: int, message: str = None):
    """Raise an exception based on the error status code from an HTTP response.

    Include additional message taken from the response bondy content.

    Parameters
    ==========
    :param status_code: Status code of the HTTP response.
    :type status_code: int
    :param message: Error message.
    :type message: str

    Raises
    ======
    :raises: :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPBadRequestError` |
             :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPUnauthorizedError` |
             :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPForbiddenError` |
             :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPNotFoundError` |
             :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPRequestTimeoutError` |
             :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPInternalServerError` |
             :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPNotImplementedError` |
             :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPBadGatewayError` |
             :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPServiceUnavailableError` |
             :py:class:`~networkandutilitiespy.exceptions.http_exceptions.HTTPUnexpectedError`
    """
    if status_code == 400:
        raise HTTPBadRequestError(message=message)
    elif status_code == 401:
        raise HTTPUnauthorizedError(message=message)
    elif status_code == 403:
        raise HTTPForbiddenError(message=message)
    elif status_code == 404:
        raise HTTPNotFoundError(message=message)
    elif status_code == 408:
        raise HTTPRequestTimeoutError(message=message)
    elif status_code == 500:
        raise HTTPInternalServerError(message=message)
    elif status_code == 501:
        raise HTTPNotImplementedError(message=message)
    elif status_code == 502:
        raise HTTPBadGatewayError(message=message)
    elif status_code == 503:
        raise HTTPServiceUnavailableError(message=message)
    else:
        raise HTTPUnexpectedError(message=message)


__all__ = (
    'raise_http_error',
)
