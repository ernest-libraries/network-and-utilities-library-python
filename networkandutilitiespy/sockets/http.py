"""HTTP client abstract class.

Classes
=======
1. HTTP
    Base abstract class for constructing HTTP requests for application-specific API requests.
"""
import json
from abc import ABC
from urllib.parse import urljoin

import requests
from requests.auth import HTTPBasicAuth

from networkandutilitiespy.exceptions import raise_http_error


class HTTP(ABC):
    """Base abstract class for constructing HTTP requests for application-specific API requests."""

    def __init__(self, base_uri='http://localhost:8080', cert=None, verify=True, timeout=30, proxies=None,
                 allow_redirects=True, session=None):
        """Create a new instance of HTTP class.

        Parameters
        ==========
        :param base_uri: Base URI for the HTTP request destination address.
        :type base_uri: str
        :param cert: Certificate for use in authenticated requests sent to the destination address. This should be a
            tuple with the certificate and then key.
        :type cert: tuple
        :param verify: Either boolean to indicate whether TLS verification should be performed when sending requests to
            the destination address, or a string pointing at the CA bundle to use for verification.
            See http://docs.python-requests.org/en/master/user/advanced/#ssl-cert-verification.
        :type verify: Union[bool,str]
        :param timeout: The timeout value for requests sent to the destination address.
        :type timeout: int
        :param proxies: Proxies to use when preforming requests.
            See: http://docs.python-requests.org/en/master/user/advanced/#proxies
        :type proxies: dict
        :param allow_redirects: Whether to follow redirects when sending requests to the destination address.
        :type allow_redirects: bool
        :param session: Optional session object to use when performing request.
        :type session: request.Session
        """
        if session is None:
            session = requests.Session()

        self.base_uri = base_uri
        self.session = session
        self.allow_redirects = allow_redirects

        self._kwargs = {
            'cert': cert,
            'verify': verify,
            'timeout': timeout,
            'proxies': proxies,
        }

        self._rpc_batch = []

        return

    def set_basic_authentication(self, username, password):
        """Set HTTP Basic Access Authentication and returns the HTTPBasicAuth object.

        Parameters
        ==========
        :param username: Username.
        :type username: str
        :param password: Password.
        :type password: str

        Returns
        =======
        :return: HTTPBasicAuth object
        :rtype: requests.HTTPBasicAuth
        """
        self._kwargs['auth'] = HTTPBasicAuth(username, password)

        return self._kwargs['auth']

    def send_rpc_request(self, request_id, method, parameters=None, json_rpc_protocol='2.0'):
        """Send a JSON-RPC request over HTTP and returns the response.

        Parameters
        ==========
        :param request_id: Request identifier.
        :type request_id: str or int or None
        :param method: Name of the method to be invoked.
        :type method: str
        :param parameters: Parameters as specified by the RPC command.
        :type parameters: list or dict
        :param json_rpc_protocol: '2.0' by default or '1.0' otherwise.
        :type json_rpc_protocol: str

        Returns
        =======
        :return: The response of the request.
        :rtype: requests.Response
        """
        headers = {
            'Content-Type': 'application/json-rpc',
        }

        payload = self.create_rpc_request(request_id, method, parameters, json_rpc_protocol)
        response = self.request(method='post', url='/', headers=headers, data=json.dumps(payload))

        return response

    def add_rpc_request_to_batch(self, request_id, method, parameters=None, json_rpc_protocol='2.0'):
        """Creates a RPC request and adds it to a batch list.

        Parameters
        ==========
        :param request_id: Request identifier.
        :type request_id: str or int or None
        :param method: Name of the method to be invoked.
        :type method: str
        :param parameters: Parameters as specified by the RPC command.
        :type parameters: list or dict
        :param json_rpc_protocol: '2.0' by default or '1.0' otherwise.
        :type json_rpc_protocol: str
        """
        rpc_request = self.create_rpc_request(request_id, method, parameters, json_rpc_protocol)

        self._rpc_batch.append(rpc_request)

        return

    def send_batch_rpc_requests(self):
        """Send stored batch JSON-RPC requests and returns the response.

        Returns
        =======
        :return: The response of the request.
        :rtype: requests.Response
        """
        if len(self._rpc_batch) == 0:
            return

        headers = {
            'Content-Type': 'application/json-rpc',
        }

        response = self.request(method='post', url='/', headers=headers, data=json.dumps(self._rpc_batch))
        self._rpc_batch = []

        return response

    @staticmethod
    def create_rpc_request(request_id, method, parameters=None, json_rpc_protocol='2.0'):
        """Creates a JSON-RPC request and returns the data structure.

        The JSON-RPC request structure is as specified in https://www.jsonrpc.org/specification.

        Parameters
        ==========
        :param request_id: Request identifier.
        :type request_id: str or int or None
        :param method: Name of the method to be invoked.
        :type method: str
        :param parameters: Parameters as specified by the RPC command.
        :type parameters: list or dict
        :param json_rpc_protocol: '2.0' by default or '1.0' otherwise.
        :type json_rpc_protocol: str

        Returns
        =======
        :return: JSON-RPC request structure.
        :rtype: dict
        """
        if parameters is None:
            if json_rpc_protocol == '2.0':
                parameters = {}
            else:
                parameters = []

        payload = {
            'method': method,
            'jsonrpc': json_rpc_protocol,
            'id': request_id,
            'params': parameters,
        }

        return payload

    def close(self):
        """Close the Requests session."""
        self.session.close()

        return

    def get(self, url, **kwargs):
        """Sends an HTTP GET request.

        Parameters
        ==========
        :param url: URL path to send the request to. This will be joined with the instance's base_uri using the
            Python's standard library function urllib.parse.urljoin(base, url, allow_fragments=True)
        :type url: str | unicode
        :param kwargs: Additional keyword arguments to include with the request.
        :type kwargs: dict

        Returns
        =======
        :return: The response of the request.
        :rtype: requests.Response
        """
        return self.request('get', url, **kwargs)

    def head(self, url, **kwargs):
        """Sends an HTTP HEAD request.

        Parameters
        ==========
        :param url: URL path to send the request to. This will be joined with the instance's base_uri using the
            Python's standard library function urllib.parse.urljoin(base, url, allow_fragments=True)
        :type url: str | unicode
        :param kwargs: Additional keyword arguments to include with the request.
        :type kwargs: dict

        Returns
        =======
        :return: The response of the request.
        :rtype: requests.Response
        """
        return self.request('head', url, **kwargs)

    def post(self, url, **kwargs):
        """Sends an HTTP POST request.

        Parameters
        ==========
        :param url: URL path to send the request to. This will be joined with the instance's base_uri using the
            Python's standard library function urllib.parse.urljoin(base, url, allow_fragments=True)
        :type url: str | unicode
        :param kwargs: Additional keyword arguments to include with the request.
        :type kwargs: dict

        Returns
        =======
        :return: The response of the request.
        :rtype: requests.Response
        """
        return self.request('post', url, **kwargs)

    def put(self, url, **kwargs):
        """Sends an HTTP PUT request.

        Parameters
        ==========
        :param url: URL path to send the request to. This will be joined with the instance's base_uri using the
            Python's standard library function urllib.parse.urljoin(base, url, allow_fragments=True)
        :type url: str | unicode
        :param kwargs: Additional keyword arguments to include with the request.
        :type kwargs: dict

        Returns
        =======
        :return: The response of the request.
        :rtype: requests.Response
        """
        return self.request('put', url, **kwargs)

    def delete(self, url, **kwargs):
        """Sends an HTTP DELETE request.

        Parameters
        ==========
        :param url: URL path to send the request to. This will be joined with the instance's base_uri using the
            Python's standard library function urllib.parse.urljoin(base, url, allow_fragments=True)
        :type url: str | unicode
        :param kwargs: Additional keyword arguments to include with the request.
        :type kwargs: dict

        Returns
        =======
        :return: The response of the request.
        :rtype: requests.Response
        """
        return self.request('delete', url, **kwargs)

    def request(self, method, url, headers=None, raise_exception=True, **kwargs):
        """Main method for routing HTTP requests.

        Parameters
        ==========
        :param method: HTTP method to use with the request e.g. GET, HEAD, POST, PUT, DELETE, etc
        :type method: str
        :param url: URL path to send the request to. This will be joined with the instance's base_uri using the
            Python's standard library function urllib.parse.urljoin(base, url, allow_fragments=True)
        :type url: str | unicode
        :param headers: Additional headers to include with the request.
        :type headers: dict
        :param kwargs: Additional keyword arguments to include with the requests.
        :type kwargs: dict
        :param raise_exception: If True, raise an exception via hvaultpy.exceptions.raise_http_error().
            Set this parameter to False to bypass this functionality.
        :type raise_exception: bool

        Returns
        =======
        :return: The response of the request.
        :rtype: requests.Response
        """
        url = urljoin(self.base_uri, url)

        _kwargs = self._kwargs.copy()
        _kwargs.update(kwargs)

        response = self.session.request(
            method=method,
            url=url,
            headers=headers,
            allow_redirects=self.allow_redirects,
            **_kwargs)

        if raise_exception and 400 <= response.status_code < 600:
            raise_http_error(response.status_code, response.content.decode('utf-8'))

        return response


__all__ = (
    'HTTP',
)
