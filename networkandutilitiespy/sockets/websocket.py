"""WebSocket client abstract class.

Classes
=======
1. WebSocket
    Base abstract class for constructing WebSocket requests for application-specific API requests.
"""
import websockets
from abc import ABC


class WebSocket(ABC):
    """Base abstract class for constructing WebSocket messages for application-specific API requests."""

    def __init__(self):
        """Create a new instance of WebSocket class."""
        self.channel = None

        return

    async def connect(self, uri, create_protocol=None, ping_interval=20, ping_timeout=20, close_timeout=10,
                      max_size=2 ** 20, max_queue=2 ** 5, read_limit=2 ** 16, write_limit=2 ** 16, loop=None,
                      compression='deflate', origin=None, extensions=None, subprotocols=None, extra_headers=None,
                      **kwds):
        """Connects to a WebSocket server.

        The parameters are taken from websockets by aaugustin. For more information, see:
        https://websockets.readthedocs.io/en/stable/api.html#module-websockets.client

        Parameters
        ==========
        :param uri: WebSocket server to connect to.
        :type uri: str
        :param create_protocol: Allows Customisation of the asyncio protocol that manages the connection.
        :type create_protocol: callable class that accepts the same arguments and return WebSocketClientProtocol.
        :param ping_interval: Sends a Ping frame for every defined interval, in seconds.
        :type ping_interval: int
        :param ping_timeout: How long to wait, in seconds, for a Pong frame until connection is unusable and closed.
        :type ping_timeout: int
        :param close_timeout: Maximum wait time, in seconds, for completing the closing handshake.
        :type close_timeout: int
        :param max_size: Maximum size for incoming messages, in Bytes. Default is 1 MB = 1,048,576 Bytes.
        :type max_size: int
        :param max_queue: Maximum length of queue that holds the incoming messages. Default is 32.
        :type max_queue: int
        :param read_limit: The high limit of the buffer for incoming Bytes. Default is 64 KB = 65,536 Bytes.
        :type read_limit: int
        :param write_limit: The high limit of the buffer for outgoing Bytes. Default is 64 KB = 65,536 Bytes.
        :type write_limit: int
        :param loop:
        :param compression: Compression extensions. Default is deflate (permessage-deflate) extension.
        :type compression: str
        :param origin: Origin HTTP header.
        :type origin: str
        :param extensions: List of supported extensions.
        :type extensions: list
        :param subprotocols: List of sub-protocols.
        :type subprotocols: list
        :param extra_headers: Additional HTTP request headers
        :type Headers | Mapping | iterable
        :param kwds:
        """
        self.channel = await websockets.connect(uri, create_protocol=create_protocol, ping_interval=ping_interval,
                                                ping_timeout=ping_timeout, close_timeout=close_timeout,
                                                max_size=max_size, max_queue=max_queue, read_limit=read_limit,
                                                write_limit=write_limit, loop=loop, compression=compression,
                                                origin=origin, extensions=extensions, subprotocols=subprotocols,
                                                extra_headers=extra_headers, **kwds)

        return

    async def send(self, data):
        """Sends a message to the WebSocket server.

        Parameters
        ==========
        :param data: Message to send.
        :type data: str | bytes | iterable of str and bytes
        """
        return await self.channel.send(data)

    async def receive(self):
        """Receives the next message from the WebSocket server."""
        return await self.channel.recv()

    async def close(self):
        """Close the channel to the WebSocket server."""
        await self.channel.close()

    async def ping(self):
        """Sends a ping to the WebSocket server."""
        return await self.channel.ping()


__all__ = [
    'WebSocket',
]
