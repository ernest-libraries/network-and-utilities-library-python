import os
import logging
import pytest
import json
from requests import Response
from requests.auth import HTTPBasicAuth

from networkandutilitiespy import utils
from networkandutilitiespy.exceptions import HTTPBadRequestError
from networkandutilitiespy.sockets import HTTP


@pytest.fixture(scope='module')
def log(config):
    log_filename = os.path.join(config['directory']['logs_path'], f'{__name__}.log')
    log = utils.custom_logger(__name__, filename=log_filename, mode='w+', log_level=logging.INFO)

    return log


@pytest.fixture(scope='module')
def http():
    return HTTP()


def test_set_basic_authentication_returns_httpbasicauth_object(http):
    auth = http.set_basic_authentication('username', 'password')

    assert type(auth) is HTTPBasicAuth


def test_request_head_httpbin_org(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.request(method='head', url='http://httpbin.org/')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_headers_json = json.dumps(dict(response.headers))
        log.info(f'Response of HEAD Request received: {response_headers_json}')

        assert response.status_code == 200 \
            and response_headers_json == '{"Content-Type": "application/json"}'


def test_request_get_httpbin_org_get(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{"args": {}, "headers": {"Accept": "*/*", "Accept-Encoding": "gzip, deflate", ' \
                            '"Connection": "close", "Host": "httpbin.org", ' \
                            '"User-Agent": "python-requests/2.21.0"}, "origin": "192.168.0.1", ' \
                            '"url": "http://httpbin.org/get"}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.request(method='get', url='http://httpbin.org/get')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_json = json.dumps(response.json())
        log.info(f'Response of GET Request received: {response_json}')

        assert response.status_code == 200 \
            and response_json == '{"args": {}, "headers": {"Accept": "*/*", "Accept-Encoding": "gzip, deflate", ' \
                                 '"Connection": "close", "Host": "httpbin.org", ' \
                                 '"User-Agent": "python-requests/2.21.0"}, "origin": "192.168.0.1", ' \
                                 '"url": "http://httpbin.org/get"}'


def test_request_post_httpbin_org_post(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{"args": {}, "data": "", "files": {}, "form": {}, "headers": ' \
                            '{"Accept": "*/*", "Accept-Encoding": "gzip, deflate", "Connection": "close", ' \
                            '"Content-Length": "0", "Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, ' \
                            '"json": null, "origin": "192.168.0.1", "url": "http://httpbin.org/post"}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.request(method='post', url='http://httpbin.org/post')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_json = json.dumps(response.json())
        log.info(f'Response of POST Request received: {response_json}')

        assert response.status_code == 200 \
            and response_json == '{"args": {}, "data": "", "files": {}, "form": {}, "headers": ' \
                                 '{"Accept": "*/*", "Accept-Encoding": "gzip, deflate", "Connection": "close", ' \
                                 '"Content-Length": "0", "Host": "httpbin.org", ' \
                                 '"User-Agent": "python-requests/2.21.0"}, "json": null, "origin": "192.168.0.1", ' \
                                 '"url": "http://httpbin.org/post"}'


def test_request_put_httpbin_org_put(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{"args": {}, "data": "", "files": {}, "form": {}, "headers": {"Accept": "*/*", ' \
                            '"Accept-Encoding": "gzip, deflate", "Connection": "close", "Content-Length": "0", ' \
                            '"Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, "json": null, ' \
                            '"origin": "192.168.0.1", "url": "http://httpbin.org/put"}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.request(method='put', url='http://httpbin.org/put')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_json = json.dumps(response.json())
        log.info(f'Response of PUT Request received: {response_json}')

        assert response.status_code == 200 \
            and response_json == '{"args": {}, "data": "", "files": {}, "form": {}, "headers": {"Accept": "*/*", ' \
                                 '"Accept-Encoding": "gzip, deflate", "Connection": "close", "Content-Length": "0", ' \
                                 '"Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, "json": null, ' \
                                 '"origin": "192.168.0.1", "url": "http://httpbin.org/put"}'


def test_request_delete_httpbin_org_delete(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{"args": {}, "data": "", "files": {}, "form": {}, "headers": {"Accept": "*/*", ' \
                            '"Accept-Encoding": "gzip, deflate", "Connection": "close", "Content-Length": "0", ' \
                            '"Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, "json": null, ' \
                            '"origin": "192.168.0.1", "url": "http://httpbin.org/delete"}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.request(method='delete', url='http://httpbin.org/delete')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_json = json.dumps(response.json())
        log.info(f'Response of DELETE Request received: {response_json}')

        assert response.status_code == 200 \
            and response_json == '{"args": {}, "data": "", "files": {}, "form": {}, "headers": {"Accept": "*/*", ' \
                                 '"Accept-Encoding": "gzip, deflate", "Connection": "close", "Content-Length": "0", ' \
                                 '"Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, "json": null, ' \
                                 '"origin": "192.168.0.1", "url": "http://httpbin.org/delete"}'


def test_head_httpbin_org(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.head(url='http://httpbin.org/')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_headers_json = json.dumps(dict(response.headers))
        log.info(f'Response of HEAD Request received: {response_headers_json}')

        assert response.status_code == 200 \
            and response_headers_json == '{"Content-Type": "application/json"}'


def test_get_httpbin_org_get(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{"args": {}, "headers": {"Accept": "*/*", "Accept-Encoding": "gzip, deflate", ' \
                            '"Connection": "close", "Host": "httpbin.org", ' \
                            '"User-Agent": "python-requests/2.21.0"}, "origin": "192.168.0.1", ' \
                            '"url": "http://httpbin.org/get"}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.get(url='http://httpbin.org/get')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_json = json.dumps(response.json())
        log.info(f'Response of GET Request received: {response_json}')

        assert response.status_code == 200 \
            and response_json == '{"args": {}, "headers": {"Accept": "*/*", "Accept-Encoding": "gzip, deflate", ' \
                                 '"Connection": "close", "Host": "httpbin.org", ' \
                                 '"User-Agent": "python-requests/2.21.0"}, "origin": "192.168.0.1", ' \
                                 '"url": "http://httpbin.org/get"}'


def test_post_httpbin_org_post(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{"args": {}, "data": "", "files": {}, "form": {}, "headers": ' \
                            '{"Accept": "*/*", "Accept-Encoding": "gzip, deflate", "Connection": "close", ' \
                            '"Content-Length": "0", "Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, ' \
                            '"json": null, "origin": "192.168.0.1", "url": "http://httpbin.org/post"}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.post(url='http://httpbin.org/post')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_json = json.dumps(response.json())
        log.info(f'Response of POST Request received: {response_json}')

        assert response.status_code == 200 \
            and response_json == '{"args": {}, "data": "", "files": {}, "form": {}, "headers": ' \
                                 '{"Accept": "*/*", "Accept-Encoding": "gzip, deflate", "Connection": "close", ' \
                                 '"Content-Length": "0", "Host": "httpbin.org", ' \
                                 '"User-Agent": "python-requests/2.21.0"}, "json": null, "origin": "192.168.0.1", ' \
                                 '"url": "http://httpbin.org/post"}'


def test_put_httpbin_org_put(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{"args": {}, "data": "", "files": {}, "form": {}, "headers": {"Accept": "*/*", ' \
                            '"Accept-Encoding": "gzip, deflate", "Connection": "close", "Content-Length": "0", ' \
                            '"Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, "json": null, ' \
                            '"origin": "192.168.0.1", "url": "http://httpbin.org/put"}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.put(url='http://httpbin.org/put')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_json = json.dumps(response.json())
        log.info(f'Response of PUT Request received: {response_json}')

        assert response.status_code == 200 \
            and response_json == '{"args": {}, "data": "", "files": {}, "form": {}, "headers": {"Accept": "*/*", ' \
                                 '"Accept-Encoding": "gzip, deflate", "Connection": "close", "Content-Length": "0", ' \
                                 '"Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, "json": null, ' \
                                 '"origin": "192.168.0.1", "url": "http://httpbin.org/put"}'


def test_delete_httpbin_org_delete(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{"args": {}, "data": "", "files": {}, "form": {}, "headers": {"Accept": "*/*", ' \
                            '"Accept-Encoding": "gzip, deflate", "Connection": "close", "Content-Length": "0", ' \
                            '"Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, "json": null, ' \
                            '"origin": "192.168.0.1", "url": "http://httpbin.org/delete"}'
    mock_response.status_code = 200
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.delete(url='http://httpbin.org/delete')
    except Exception as error:
        log.error(f'{error}')

        assert False
    else:
        response_json = json.dumps(response.json())
        log.info(f'Response of DELETE Request received: {response_json}')

        assert response.status_code == 200 \
            and response_json == '{"args": {}, "data": "", "files": {}, "form": {}, "headers": {"Accept": "*/*", ' \
                                 '"Accept-Encoding": "gzip, deflate", "Connection": "close", "Content-Length": "0", ' \
                                 '"Host": "httpbin.org", "User-Agent": "python-requests/2.21.0"}, "json": null, ' \
                                 '"origin": "192.168.0.1", "url": "http://httpbin.org/delete"}'


def test_head_httpbin_org_raise_400_error(http, mocker, log):
    mock_response = Response()

    mock_response_content = '{}'
    mock_response.status_code = 400
    mock_response.headers['Content-Type'] = 'application/json'
    mock_response._content = bytearray(mock_response_content, 'utf-8')
    mocker.patch('requests.Session.request', return_value=mock_response)

    try:
        response = http.head(url='http://httpbin.org/')
    except HTTPBadRequestError as error:
        log.error(f'{error.message}')

        assert error.message == '400 Bad Request, {}'
    else:
        response_headers_json = json.dumps(dict(response.headers))
        log.info(f'Response of HEAD Request received: {response_headers_json}')

        assert response.status_code == 400
