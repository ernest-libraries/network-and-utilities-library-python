import os
import pytest
import logging

from networkandutilitiespy import exceptions
from networkandutilitiespy import utils


@pytest.fixture(scope='module')
def log(config):
    log_filename = os.path.join(config['directory']['logs_path'], f'{__name__}.log')
    log = utils.custom_logger(__name__, filename=log_filename, mode='w+', log_level=logging.INFO)

    return log


def test_requests_error_read_timeout_blank_request(log):
    try:
        raise exceptions.ReadTimeout()
    except exceptions.ReadTimeout as error:
        log.error(f'Request: {error.request}; Response: {error.response}')

        assert error.request is None and error.response is None
