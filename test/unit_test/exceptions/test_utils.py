import os
import pytest
import logging

from networkandutilitiespy import exceptions
from networkandutilitiespy import utils


@pytest.fixture(scope='module')
def log(config):
    log_filename = os.path.join(config['directory']['logs_path'], f'{__name__}.log')
    log = utils.custom_logger(__name__, filename=log_filename, mode='w+', log_level=logging.INFO)

    return log


class Parent:
    attribute_1 = utils.AbstractAttribute()


def test_abstract_attribute_value_not_set(log):
    parent = Parent()

    try:
        log.info(f'Attribute value is set to: {parent.attribute_1}')
    except NotImplementedError as error:
        log.error(f'{error}')

        assert True
    else:
        assert False


def test_abstract_attribute_value_set(log):
    parent = Parent()

    parent.attribute_1 = 2
    try:
        log.info(f'Attribute value is set to: {parent.attribute_1}')
    except NotImplementedError as error:
        log.error(f'{error}')

        assert False
    else:
        assert True


def test_raise_http_error_400(log):
    try:
        raise exceptions.raise_http_error(status_code=400)
    except exceptions.HTTPBadRequestError as error:
        log.error(f'{error.message}')

        assert error.status_code == 400 and error.message == '400 Bad Request'


def test_raise_http_error_401(log):
    try:
        raise exceptions.raise_http_error(status_code=401)
    except exceptions.HTTPUnauthorizedError as error:
        log.error(f'{error.message}')

        assert error.status_code == 401 and error.message == '401 Unauthorized'


def test_raise_http_error_403(log):
    try:
        raise exceptions.raise_http_error(status_code=403)
    except exceptions.HTTPForbiddenError as error:
        log.error(f'{error.message}')

        assert error.status_code == 403 and error.message == '403 Forbidden'


def test_raise_http_error_404(log):
    try:
        raise exceptions.raise_http_error(status_code=404)
    except exceptions.HTTPNotFoundError as error:
        log.error(f'{error.message}')

        assert error.status_code == 404 and error.message == '404 Not Found'


def test_raise_http_error_408(log):
    try:
        raise exceptions.raise_http_error(status_code=408)
    except exceptions.HTTPRequestTimeoutError as error:
        log.error(f'{error.message}')

        assert error.status_code == 408 and error.message == '408 Request Timeout'


def test_raise_http_error_500(log):
    try:
        raise exceptions.raise_http_error(status_code=500)
    except exceptions.HTTPInternalServerError as error:
        log.error(f'{error.message}')

        assert error.status_code == 500 and error.message == '500 Internal Server Error'


def test_raise_http_error_501(log):
    try:
        raise exceptions.raise_http_error(status_code=501)
    except exceptions.HTTPNotImplementedError as error:
        log.error(f'{error.message}')

        assert error.status_code == 501 and error.message == '501 Not Implemented'


def test_raise_http_error_502(log):
    try:
        raise exceptions.raise_http_error(status_code=502)
    except exceptions.HTTPBadGatewayError as error:
        log.error(f'{error.message}')

        assert error.status_code == 502 and error.message == '502 Bad Gateway'


def test_raise_http_error_503(log):
    try:
        raise exceptions.raise_http_error(status_code=503)
    except exceptions.HTTPServiceUnavailableError as error:
        log.error(f'{error.message}')

        assert error.status_code == 503 and error.message == '503 Service Unavailable'


def test_raise_http_error_unexpected(log):
    try:
        raise exceptions.raise_http_error(status_code=0)
    except exceptions.HTTPUnexpectedError as error:
        log.error(f'{error.message}')

        assert error.status_code == 0 and error.message == '0 Unexpected Error'
