import os
import pytest
import logging

from networkandutilitiespy import exceptions
from networkandutilitiespy import utils


@pytest.fixture(scope='module')
def log(config):
    log_filename = os.path.join(config['directory']['logs_path'], f'{__name__}.log')
    log = utils.custom_logger(__name__, filename=log_filename, mode='w+', log_level=logging.INFO)

    return log


def test_http_error_no_custom_errors(log):
    try:
        raise exceptions.HTTPError(status_code=100, message='This is an error message')
    except exceptions.HTTPError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 100 and error.message == 'This is an error message')


def test_http_error_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPError(status_code=100, message='This is an error message', errors=['error_1', 'error_2'])
    except exceptions.HTTPError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 100 and error.message == 'This is an error message: error_1, error_2')


def test_http_error_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPError(status_code=100, message='This is an error message', errors=('error_1', 'error_2'))
    except exceptions.HTTPError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 100 and error.message == 'This is an error message: error_1, error_2')


def test_http_bad_request_no_custom_errors(log):
    try:
        raise exceptions.HTTPBadRequestError()
    except exceptions.HTTPBadRequestError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 400 and error.message == '400 Bad Request')


def test_http_bad_request_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPBadRequestError(errors=['error_1', 'error_2'])
    except exceptions.HTTPBadRequestError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 400 and error.message == '400 Bad Request: error_1, error_2')


def test_http_bad_request_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPBadRequestError(errors=('error_1', 'error_2'))
    except exceptions.HTTPBadRequestError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 400 and error.message == '400 Bad Request: error_1, error_2')


def test_http_unauthorized_no_custom_errors(log):
    try:
        raise exceptions.HTTPUnauthorizedError()
    except exceptions.HTTPUnauthorizedError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 401 and error.message == '401 Unauthorized')


def test_http_unauthorized_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPUnauthorizedError(errors=['error_1', 'error_2'])
    except exceptions.HTTPUnauthorizedError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 401 and error.message == '401 Unauthorized: error_1, error_2')


def test_http_unauthorized_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPUnauthorizedError(errors=('error_1', 'error_2'))
    except exceptions.HTTPUnauthorizedError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 401 and error.message == '401 Unauthorized: error_1, error_2')


def test_http_forbidden_no_custom_errors(log):
    try:
        raise exceptions.HTTPForbiddenError()
    except exceptions.HTTPForbiddenError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 403 and error.message == '403 Forbidden')


def test_http_forbidden_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPForbiddenError(errors=['error_1', 'error_2'])
    except exceptions.HTTPForbiddenError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 403 and error.message == '403 Forbidden: error_1, error_2')


def test_http_forbidden_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPForbiddenError(errors=('error_1', 'error_2'))
    except exceptions.HTTPForbiddenError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 403 and error.message == '403 Forbidden: error_1, error_2')


def test_http_not_found_no_custom_errors(log):
    try:
        raise exceptions.HTTPNotFoundError()
    except exceptions.HTTPNotFoundError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 404 and error.message == '404 Not Found')


def test_http_not_found_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPNotFoundError(errors=['error_1', 'error_2'])
    except exceptions.HTTPNotFoundError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 404 and error.message == '404 Not Found: error_1, error_2')


def test_http_not_found_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPNotFoundError(errors=('error_1', 'error_2'))
    except exceptions.HTTPNotFoundError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 404 and error.message == '404 Not Found: error_1, error_2')


def test_http_request_timeout_no_custom_errors(log):
    try:
        raise exceptions.HTTPRequestTimeoutError()
    except exceptions.HTTPRequestTimeoutError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 408 and error.message == '408 Request Timeout')


def test_http_request_timeout_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPRequestTimeoutError(errors=['error_1', 'error_2'])
    except exceptions.HTTPRequestTimeoutError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 408 and error.message == '408 Request Timeout: error_1, error_2')


def test_http_request_timeout_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPRequestTimeoutError(errors=('error_1', 'error_2'))
    except exceptions.HTTPRequestTimeoutError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 408 and error.message == '408 Request Timeout: error_1, error_2')


def test_http_internal_server_error_no_custom_errors(log):
    try:
        raise exceptions.HTTPInternalServerError()
    except exceptions.HTTPInternalServerError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 500 and error.message == '500 Internal Server Error')


def test_http_internal_server_error_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPInternalServerError(errors=['error_1', 'error_2'])
    except exceptions.HTTPInternalServerError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 500 and error.message == '500 Internal Server Error: error_1, error_2')


def test_http_internal_server_error_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPInternalServerError(errors=('error_1', 'error_2'))
    except exceptions.HTTPInternalServerError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 500 and error.message == '500 Internal Server Error: error_1, error_2')


def test_http_not_implemented_error_no_custom_errors(log):
    try:
        raise exceptions.HTTPNotImplementedError()
    except exceptions.HTTPNotImplementedError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 501 and error.message == '501 Not Implemented')


def test_http_not_implemented_error_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPNotImplementedError(errors=['error_1', 'error_2'])
    except exceptions.HTTPNotImplementedError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 501 and error.message == '501 Not Implemented: error_1, error_2')


def test_http_not_implemented_error_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPNotImplementedError(errors=('error_1', 'error_2'))
    except exceptions.HTTPNotImplementedError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 501 and error.message == '501 Not Implemented: error_1, error_2')


def test_http_bad_gateway_error_no_custom_errors(log):
    try:
        raise exceptions.HTTPBadGatewayError()
    except exceptions.HTTPBadGatewayError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 502 and error.message == '502 Bad Gateway')


def test_http_bad_gateway_error_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPBadGatewayError(errors=['error_1', 'error_2'])
    except exceptions.HTTPBadGatewayError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 502 and error.message == '502 Bad Gateway: error_1, error_2')


def test_http_bad_gateway_error_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPBadGatewayError(errors=('error_1', 'error_2'))
    except exceptions.HTTPBadGatewayError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 502 and error.message == '502 Bad Gateway: error_1, error_2')


def test_http_service_unavailable_error_no_custom_errors(log):
    try:
        raise exceptions.HTTPServiceUnavailableError()
    except exceptions.HTTPServiceUnavailableError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 503 and error.message == '503 Service Unavailable')


def test_http_service_unavailable_error_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPServiceUnavailableError(errors=['error_1', 'error_2'])
    except exceptions.HTTPServiceUnavailableError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 503 and error.message == '503 Service Unavailable: error_1, error_2')


def test_http_service_unavailable_error_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPServiceUnavailableError(errors=('error_1', 'error_2'))
    except exceptions.HTTPServiceUnavailableError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 503 and error.message == '503 Service Unavailable: error_1, error_2')


def test_http_unexpected_error_no_custom_errors(log):
    try:
        raise exceptions.HTTPUnexpectedError()
    except exceptions.HTTPUnexpectedError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 0 and error.message == '0 Unexpected Error')


def test_http_unexpected_error_with_custom_list_errors(log):
    try:
        raise exceptions.HTTPUnexpectedError(errors=['error_1', 'error_2'])
    except exceptions.HTTPUnexpectedError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 0 and error.message == '0 Unexpected Error: error_1, error_2')


def test_http_unexpected_error_with_custom_tuple_errors(log):
    try:
        raise exceptions.HTTPUnexpectedError(errors=('error_1', 'error_2'))
    except exceptions.HTTPUnexpectedError as error:
        log.error(f'{error.message}')

        assert (error.status_code == 0 and error.message == '0 Unexpected Error: error_1, error_2')
