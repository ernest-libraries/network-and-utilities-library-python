import pytest
import configparser


@pytest.fixture(scope='module')
def config():
    config = configparser.ConfigParser()
    config.read('./test/unit_test/unit.ini')

    return config
