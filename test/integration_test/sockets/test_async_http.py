import os
import logging
import pytest

from networkandutilitiespy import utils
from networkandutilitiespy.sockets import HTTP


@pytest.fixture(scope='module')
def log(config):
    log_filename = os.path.join(config['directory']['logs_path'], f'{__name__}.log')
    log = utils.custom_logger(__name__, filename=log_filename, mode='w+', log_level=logging.INFO)

    return log


@pytest.mark.asyncio
async def test_http_async_wrapper_get_request(log):
    http = HTTP('http://httpbin.org')

    response = await http.get_async('/get')
    log.info(await response.text())

    await http.close_async()


@pytest.mark.asyncio
async def test_http_async_wrapper_post_request(log):
    http = HTTP('http://httpbin.org')

    response = await http.post_async('/post', data=b'data')
    log.info(await response.text())

    await http.close_async()
