import os
import logging
import pytest

from networkandutilitiespy import utils
from networkandutilitiespy.sockets import WebSocket


@pytest.fixture(scope='module')
def log(config):
    log_filename = os.path.join(config['directory']['logs_path'], f'{__name__}.log')
    log = utils.custom_logger(__name__, filename=log_filename, mode='w+', log_level=logging.INFO)

    return log


@pytest.mark.asyncio
async def test_websocket_wrapper_hello_world_send_and_receive(config, log):
    uri = config['websocket']['base_uri']
    websocket = WebSocket()

    await websocket.connect(uri=uri)

    message = 'Hello World.'
    log.info(f'Sending Websocket Message to {uri}: {message}...')
    await websocket.send(message)

    response = await websocket.receive()
    log.info(f'Response: {response}')
    await websocket.close()

    assert response == 'Hello World.'


@pytest.mark.asyncio
async def test_secure_websocket_wrapper_hello_world_send_and_receive(config, log):
    uri = config['websocket']['secure_base_uri']
    websocket = WebSocket()

    await websocket.connect(uri=uri)

    message = 'Hello Secure World.'
    log.info(f'Sending Websocket Message to {uri}: {message}...')
    await websocket.send(message)

    response = await websocket.receive()
    log.info(f'Response: {response}')
    await websocket.close()

    assert response == 'Hello Secure World.'
