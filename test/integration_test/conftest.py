import pytest
import configparser


@pytest.fixture(scope='module')
def config():
    config = configparser.ConfigParser()
    config.read('./test/integration_test/integration.ini')

    return config
