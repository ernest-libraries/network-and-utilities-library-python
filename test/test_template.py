import os
import pytest
import logging
from networkandutilitiespy import utils

# Specific section of library to be tested
# from networkandutilitiespy import exceptions


@pytest.fixture(scope='module')
def log(config):
    log_filename = os.path.join(config['directory']['logs_path'], f'{__name__}.log')
    log = utils.custom_logger(__name__, filename=log_filename, mode='w+', log_level=logging.INFO)

    return log

# Write your test to perform
# def test_http_error_no_custom_errors(log):
#     try:
#         raise exceptions.HTTPError(status_code=100, message='This is an error message')
#     except exceptions.HTTPError as error:
#         log.error(f'{error.message}')
#
#         assert (error.status_code == 100 and error.message == 'This is an error message')
