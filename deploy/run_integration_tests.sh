#!/usr/bin/env bash

rm networkandutilitiespy.log
rm -r ./test/integration_test/logs/*
python3 -m pytest --cov-report term-missing --cov=networkandutilitiespy --tb=native test/integration_test/
