from setuptools import setup, find_packages

version = {}
with open("networkandutilitiespy/version.py") as file:
    exec(file.read(), version)

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="networkandutilitiespy",
    version=version['__version__'],
    author="Ernest Yuen",
    author_email="ernestyuen08@gmail.com",
    description="Network Facade and Utilities Library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ernest-libraries/network-and-utilities-library-python",
    packages=find_packages(),
    install_requires=[
        'requests',
        'aiohttp',
        'websockets',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
